import Login from './Component/Login/Login'
import { Route , Switch } from "react-router-dom";
import Home from "./Component/Homepage/Homepage";
import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>      
        <Route path='/Login' exact component={Login} />    
        <Route path='/Homepage' exact component={Home} />    
      </Switch>
    </div>
  );
} 

export default App;
