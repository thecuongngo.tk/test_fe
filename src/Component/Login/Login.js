import  React from 'react';
import Cookies from 'js-cookie';



import './Login.css';

function Login() {
  async function onLogin(){
    let username = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    if(username === 'cuong123@gmail.com' && password ==='cuong'){
      window.location.href = '/Homepage'; 
    }
    else{
      alert('Sai tên đăng nhập hoặc mật khẩu');
      return
    }
    Cookies.set("username" , username)
  }
   
     return (
         <div className="container">
         <form action method="POST" className="form" id="form-1">
           
           <div className="container__login" >
             <div className="container__login-overlay">
               <div className="container__form">
                 <div className="container__login-text">Login</div>
                 <div className="container__login-input">
                   <div className="formInput">
                     <input type="text" className="form-control" id="email" name="email" placeholder="Tên đăng nhập" />
                     <span className="form-message" />
                   </div>
                   <div className="formInput">
                     <input type="password" className="form-control" id="password" name="password" placeholder="Mật khẩu" />
                     <span className="form-message" />
                   </div>
                   <div className="container__login-submit">
                     <button onClick={onLogin} type="button" name="submit">
                       Login
                       <i className="fas fa-chevron-right" />
                     </button>
                   </div>
                 </div>
               </div>
             </div>
           </div>
         </form>
       </div>
     );
 }
 
 export default Login;