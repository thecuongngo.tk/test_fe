import React from 'react';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';

import './Homepage.css';

function Home() {
    let [data, setData] = useState([])
    useEffect(() => {
        async function getData() {
            let response = await axios.get(`http://localhost:3000/pet`)
            //console.log(response.data);
            setData(response.data.list)

        }
        getData()
    }, [])
    function onLogout() {
        let btnLogout = document.getElementById('logout')
        btnLogout.onclick = () => {
            Cookies.remove('username')
            window.location.reload()
        }
    }

        
    
    if (Cookies.get('username')) {
        return (
            <div className="container">
                <div className="menu">
                    <p id="text-menu">petfinder</p>
                    <button id="logout" onClick={onLogout}>Logout</button>
                </div>
                {data.map(i =>
                    <div className="Card">
                        <div className="Card-left">
                            <img className="Card-left__img" src={i.img} alt="" />
                        </div>
                        <div className="Card-right">
                            <div><h2 className="Card-right__tittle"><a href>{i.name}</a></h2></div>
                            <div className="Card-right__infor">
                                <div className="Card-right__gender">{i.gender}</div>
                                <div className="Card-rifght__discription">{i.discription}</div>
                            </div>
                        </div>
                    </div>
                )}
                <div className="flex">
                    <ul>
                        <li>1</li>
                        <li>2</li>
                        <li>3</li>
                        <div className="bar" />
                    </ul>
                </div>



            </div>
            
        )
    }
    else {
        window.location.href = '/Login'
    }



}
export default Home;